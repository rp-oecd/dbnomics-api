FROM python:3.8

RUN mkdir -p /opt
RUN mkdir -p /json-data
WORKDIR /opt
COPY dbnomics-api .
COPY application.cfg ./
RUN mkdir instance
COPY application.cfg instance/
RUN pip install --requirement requirements.txt
RUN pip install --no-cache-dir gunicorn==20.0.4
EXPOSE 5000
CMD gunicorn --bind 0.0.0.0:5000 dbnomics_api.app:app
